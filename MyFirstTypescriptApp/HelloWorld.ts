class Startup {
    public static main(casual: boolean): number {
        let name: string = 'students';
        let greet: string = 'Hello ';
        if(casual) {
            greet = 'Hi ';
        }
        console.log(greet + name);
        return 0;
    }
}

Startup.main(true);

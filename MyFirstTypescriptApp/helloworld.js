var Startup = /** @class */ (function () {
    function Startup() {
    }
    Startup.main = function (casual) {
        var name = 'students';
        var greet = 'Hello ';
        if (casual) {
            greet = 'Hi ';
        }
        console.log(greet + name);
        return 0;
    };
    return Startup;
}());
Startup.main(true);
//# sourceMappingURL=helloworld.js.map
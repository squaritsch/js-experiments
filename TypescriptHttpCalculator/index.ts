import * as express from 'express';
import { Request, Response, Router, Application } from 'express';

class Calculator {
    constructor(readonly router: Router, readonly express: Application) {
        this.setupRoutes()
    }
    private setupRoutes(): void {
        this.router.get('/calc', (req: Request, res: Response) => {
            res.json({
                result: this.calculate(req)
            })
        })
        this.express.use(express.static('public'));
        this.express.use('/', this.router);
    }
    private calculate(req: Request): number {
        let op1 = Number(req.query.op1);
        let op2 = Number(req.query.op2);
        let op = req.query.oper;
        switch(op) {
            case '+': return op1 + op2;
            case '-': return op1 - op2;
            case '*': return op1 * op2;
            case '/': return op1 / op2;
        }
        return  0;
    }
}

const port = process.env.PORT || 3000;

let calc: Calculator = new Calculator(express.Router(), express());
calc.express.listen(port);
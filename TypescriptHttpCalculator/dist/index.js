"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const express = require("express");
class Calculator {
    constructor(router, express) {
        this.router = router;
        this.express = express;
        this.setupRoutes();
    }
    setupRoutes() {
        this.router.get('/calc', (req, res) => {
            res.json({
                result: this.calculate(req)
            });
        });
        this.express.use(express.static('public'));
        this.express.use('/', this.router);
    }
    calculate(req) {
        let op1 = Number(req.query.op1);
        let op2 = Number(req.query.op2);
        let op = req.query.oper;
        switch (op) {
            case '+': return op1 + op2;
            case '-': return op1 - op2;
            case '*': return op1 * op2;
            case '/': return op1 / op2;
        }
        return 0;
    }
}
const port = process.env.PORT || 3000;
let calc = new Calculator(express.Router(), express());
calc.express.listen(port);
//# sourceMappingURL=index.js.map
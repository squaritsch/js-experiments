let http = require('http');
http.createServer(function(req, res) {
    let baseUrl = 'http://' + req.headers.host + '/'; 
    let url = new URL(req.url, baseUrl);
    res.writeHead(200, {'Content-type': 'text/html'});
    res.write(url.pathname + "<br>");
    url.searchParams.forEach((v,k) => {
        res.write(k + " = " + v + "<br>");
    });
    res.end("Hello World!");
}).listen(8080);
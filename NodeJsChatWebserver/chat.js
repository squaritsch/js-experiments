let http = require('http');
let fs = require('fs');
const { FILE } = require('dns');

function ChatObj() {
    this.messages = [];
}

function Message(user, msg) {
    this.user = user;
    this.msg = msg;
}

const FILENAME = '../data/chat.dat';

http.createServer(function(req, res) {
    let baseUrl = 'http://' + req.headers.host + '/'; 
    let url = new URL(req.url, baseUrl);
    res.writeHead(200, {'Content-type': 'text/html', 
    'Access-Control-Allow-Origin': '*'});


    fs.readFile(FILENAME, 'utf8', function(err, data) {
        let chatObj;
        if(err) {
            chatObj = new ChatObj();
        } else {
            chatObj = JSON.parse(data);
        }

        let params = url.searchParams;
        msg = new Message(params.get('user'), params.get('msg'));
        chatObj.messages.push(msg);

        fs.writeFile(FILENAME, JSON.stringify(chatObj), 'utf8', function(err) {
            if(err) throw err;
            console.log("Msg saved");
        })

        chatObj.messages.forEach(msg => {
            res.write(msg.user + ": " + msg.msg + "<br>");
        });
        res.end();
    });


}).listen(8080);
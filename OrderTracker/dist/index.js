"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const express = require("express");
const OrderController_1 = require("./api/controllers/OrderController");
const OrderRoutes_1 = require("./api/routes/OrderRoutes");
const mongoose = require("mongoose");
const nconf = require("nconf");
const bodyParser = require("body-parser");
const audit = require("express-requests-logger");
class App {
    static main(port) {
        const app = express();
        //const bodyParser = require('body-parser')
        nconf.argv().env().file("config.json");
        const dbUrl = nconf.get('dbUrl');
        if (!dbUrl) {
            console.error("Missing configuration `dbUrl`");
            return;
        }
        mongoose.connect(dbUrl, { useNewUrlParser: true, useUnifiedTopology: true, useFindAndModify: false });
        app.use(bodyParser.urlencoded({ extended: true }));
        app.use(bodyParser.json());
        app.use(audit());
        let cont = new OrderController_1.OrderController();
        let routes = new OrderRoutes_1.OrderRoutes(app, cont);
        routes.setup();
        app.listen(port, (err) => {
            if (err) {
                return console.log(err);
            }
            return console.log(`server is listening on ${port}`);
        });
    }
}
App.main(3000);
//# sourceMappingURL=index.js.map
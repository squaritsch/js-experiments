"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.OrderRoutes = void 0;
const cors = require("cors");
class OrderRoutes {
    constructor(express, controller) {
        this.express = express;
        this.controller = controller;
        this.ALLOWED_CLIENT_ORIGIN_URL = "http://localhost:4200";
    }
    setup() {
        this.setCORSOptions();
        this.express.route('/orders')
            .get(this.controller.findAll)
            .post(this.controller.create);
        this.express.route('/orders/:id')
            .get(this.controller.findById)
            .put(this.controller.update)
            .delete(this.controller.delete);
    }
    setCORSOptions() {
        //options for cors midddleware
        const options = {
            allowedHeaders: ["Origin", "X-Requested-With", "Content-Type", "Accept", "X-Access-Token"],
            credentials: false,
            methods: "GET,HEAD,PUT,PATCH,POST,DELETE",
            origin: this.ALLOWED_CLIENT_ORIGIN_URL,
            preflightContinue: false
        };
        //use cors middleware
        this.express.use(cors(options));
        //enable pre-flight (not required for this example)
        this.express.options("*", cors(options));
    }
}
exports.OrderRoutes = OrderRoutes;
//# sourceMappingURL=OrderRoutes.js.map
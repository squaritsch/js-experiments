"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.OrderModel = void 0;
const mongoose_1 = require("mongoose");
const OrderSchema = new mongoose_1.Schema({
    date: {
        type: Date,
        required: true
    },
    shop: {
        type: String,
        required: true
    },
    description: {
        type: String
    },
    payedVia: {
        type: String
    },
    amount: {
        type: Number
    },
    status: {
        type: String,
        enum: ['open', 'received', 'done'],
        default: 'open'
    },
    amountRefund: {
        type: Number
    }
});
exports.OrderModel = mongoose_1.model('Order', OrderSchema);
//# sourceMappingURL=OrderModel.js.map
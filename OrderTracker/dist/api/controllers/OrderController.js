"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.OrderController = void 0;
const OrderModel_1 = require("../models/OrderModel");
class OrderController {
    findAll(req, res) {
        OrderModel_1.OrderModel.find({}, (err, orders) => {
            if (err) {
                res.sendStatus(404);
                return;
            }
            else {
                res.json(orders);
            }
        });
    }
    findById(req, res) {
        let id = req.params["id"];
        OrderModel_1.OrderModel.findById(id, (err, order) => {
            if (err || order == null) {
                res.sendStatus(404);
                return;
            }
            else {
                res.json(order);
            }
        });
    }
    create(req, res) {
        let order = req.body;
        OrderModel_1.OrderModel.create(order).then((newOrder) => {
            res.send({
                "status": "success",
                "_id": newOrder._id
            });
        });
    }
    update(req, res) {
        let id = req.params["id"];
        let order = req.body;
        OrderModel_1.OrderModel.findByIdAndUpdate(id, order, (err, found) => {
            if (err || !found) {
                res.sendStatus(404);
                return;
            }
            else {
                res.send({
                    "status": "success"
                });
            }
        });
    }
    delete(req, res) {
        let id = req.params["id"];
        OrderModel_1.OrderModel.findByIdAndDelete(id, (err, resp) => {
            if (err || !resp) {
                res.sendStatus(404);
                return;
            }
            else {
                res.send({
                    "status": "success"
                });
            }
        });
    }
}
exports.OrderController = OrderController;
//# sourceMappingURL=OrderController.js.map